const formEl = document.getElementById('movie-review-form');
const movieEl = document.getElementById('movie');
const yearEl = document.getElementById('year');

document.getElementById('movie').setAttribute('placeholder', localStorage.getItem('placeholderMovie'));
document.getElementById('year').setAttribute('placeholder', localStorage.getItem('placeholderYear'));

formEl.addEventListener('submit', function(e) {
  e.preventDefault();
  document.getElementById('movie-review').innerText = '';
  //document.getElementById('loading-notice').classList.remove('hide');

  const movie = movieEl.value.trim();
  const year = yearEl.value.trim();
  console.log(movie);

  if (movie.length < 1) {
    e.preventDefault();
    document.querySelector('.movie').classList.remove('hide');  
  } 
  if (year.length !== 4 || isNaN(year)) {
    e.preventDefault();
    document.querySelector('.year').classList.remove('hide');  
  } 
  if (movie.length >= 1 && year.length === 4) {
    document.querySelector('.movie').classList.add('hide');
    document.querySelector('.year').classList.add('hide');
    document.getElementById('loading-notice').classList.remove('hide');
  }

  const url = `https://api.nytimes.com/svc/movies/v2/reviews/search.json?query=${movie}&opening-date=${year}-01-01&api-key=ZPvnAwkWSiPA01MolWzytIifRi5P9csW`;
  if (movie != '') {
    document.querySelector('.movie').classList.add('hide');
    setTimeout( () => {
      document.getElementById('loading-notice').classList.add('hide');
    }, 2000
    )
    fetch(url)
      .then(function(data) {
        return data.json();
      })
      .then(function(responseJson) {
        if (responseJson.num_results === 0) {
          setTimeout( () =>{
          document.getElementById('movie-review').innerText += 'No results found - please try again.'
          }, 2000
          )
        } else {
          for (var i = 0; i < responseJson.num_results; i++) {
            let movieReview = responseJson.results[i];
            const headline = movieReview.headline;
            const summary = movieReview.summary_short;
            const a = document.createElement('a');
            const link = movieReview.link.url;
            a.setAttribute('href', link);
            a.innerHTML = headline;
            setTimeout ( () => {
            document.getElementById('movie-review').appendChild(a);
            const linebreak = document.createElement('br');
            document.getElementById('movie-review').appendChild(linebreak);
            const description = document.createElement('p'); 
            description.innerHTML = summary;
            document.getElementById('movie-review').appendChild(description);
            }, 2000
            )
          }
        }
    })
  }

  const placeholderMovie = document.getElementById('movie').getAttribute('placeholder');
  const placeholderYear = document.getElementById('year').getAttribute('placeholder');
  localStorage.setItem('placeholderMovie', movie);
  localStorage.setItem('placeholderYear', year);
});

